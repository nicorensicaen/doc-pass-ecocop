# Ecocop - Documentation technique

## Structure du projet

![Schématisation de la structure du projet](Raspberry PI.png "Structure du projet")

### Serveur Back-End

Le serveur a pour objectif d'établir le comportement de l'usager en récupérant les données d'entrée des capteurs et d'exposer des services afin que les données relatives au comportement de l'usager soient accessibles depuis l'extérieur (dans le cadre de notre application depuis une application Web).
Le language Go a été choisi pour la facilité de mise en oeuvre d'une application fournissant des services sous forme d'API REST. Dans son état actuel, le serveur nécessite en entrée des documents comportant : des données GPS, des données de distance provenant d'un capteur, ainsi qu'un document comportant les différents avantages disponibles pour le client en fonction de son score actuel.

Le serveur conserve aussi un journal des infractions commises afin que l'utilisateur puisse comprendre les fluctuations de son score. Ce journal est cependant encore contenu dans une variable dynamique et non une base de données.

Le serveur établit une connexion régulière avec l'API SnapToRoad de Bing (Microsoft - https://www.microsoft.com/en-us/maps/snap-to-road) afin de récupérer la limitation de vitesse en fonction des coordonnées GPS du véhicule. 

Pour démarrer le serveur, il y a deux possiblités :
En étant localisé dans le répertoire source, la commande ```go run .```

ou l'exécution du fichier (binaire généré par go avec la commande ```go build```).

Le serveur sert les services REST sur le port 10000 en réseau local et les appels possibles sont les suivants :
- ```localhost:10000/ecoscore``` pour récupérer le score de l'utilisateur
- ```localhost:10000/log``` pour obtenir le journal des 100 dernières infractions commises par l'utilisateur
- ```localhost:10000/advantages``` pour récupérer les avantages disponibles avec l'écoscore de l'utilisateur.

### Application Web

L'application Web a pour objectifs :
- D'afficher les informations utiles à l'utilisateur en relation avec sa conduite et son influence sur l'écoscore (affichage du score et des infractions commises)
- De générer un QRCode qui sera lu par des bornes appartenant aux services routiers (parkings, stations-services, drives) afin que les avantages dépendant de l'écoscore soient appliqués lors des paiements effectués par l'utilisateur.

L'application Web est exposée par une plateforme Node-JS et la forme du site a été réalisée à l'aide du patron https://www.bootstrapdash.com/bootstrap-admin-template/. Le site est constitué de code HTML, CSS et Javascript.

L'application a trois modules principaux : la phase d'authentification, la page de tableau de bord et la page de génération de QRCode.

L'authentification est réalisée par le module Firebase au travers d'un compte Google.

La page de tableau de bord met en forme lisible les données d'écoscore, son évolution ainsi que les infractions commises et leur influence sur l'écoscore. Pour récupérer les données, l'application Web fait appel aux services du serveur Go par l'intermédiaire de requêtes HTTP.

La page de génération de QRCode génère automatiquement une image de QRCode contenant le score de l'utilisateur et la date de génération du QRCode (pour limiter sa durée d'utilisation). Une librairie dédiée a été incluse pour générer le QRCode en Javascript (https://davidshimjs.github.io/qrcodejs/)

### Script du capteur

Un capteur de mesure de distances (HC-SR04) a été employé pour mesurer la distance de sécurité avec le véhicule devant celui de l'utilisateur, la vitesse étant estimé dans le programme Go.

Un script Python permet de récupérer et mettre en forme les données provenant du capteur, ainsi que de les écrire dans un fichier.
